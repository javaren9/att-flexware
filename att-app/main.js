(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/_directives/alert.component.html":
/*!**************************************************!*\
  !*** ./src/app/_directives/alert.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"message\" [ngClass]=\"{ 'alert': message, 'alert-success': message.type === 'success', 'alert-danger': message.type === 'error' }\">{{message.text}}</div>"

/***/ }),

/***/ "./src/app/_directives/alert.component.ts":
/*!************************************************!*\
  !*** ./src/app/_directives/alert.component.ts ***!
  \************************************************/
/*! exports provided: AlertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return AlertComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AlertComponent = /** @class */ (function () {
    function AlertComponent(alertService) {
        this.alertService = alertService;
    }
    AlertComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.alertService.getMessage().subscribe(function (message) {
            _this.message = message;
        });
    };
    AlertComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    AlertComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'alert',
            template: __webpack_require__(/*! ./alert.component.html */ "./src/app/_directives/alert.component.html")
        }),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_1__["AlertService"]])
    ], AlertComponent);
    return AlertComponent;
}());



/***/ }),

/***/ "./src/app/_directives/index.ts":
/*!**************************************!*\
  !*** ./src/app/_directives/index.ts ***!
  \**************************************/
/*! exports provided: AlertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _alert_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./alert.component */ "./src/app/_directives/alert.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return _alert_component__WEBPACK_IMPORTED_MODULE_0__["AlertComponent"]; });




/***/ }),

/***/ "./src/app/_guards/auth.guard.ts":
/*!***************************************!*\
  !*** ./src/app/_guards/auth.guard.ts ***!
  \***************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/_guards/index.ts":
/*!**********************************!*\
  !*** ./src/app/_guards/index.ts ***!
  \**********************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./auth.guard */ "./src/app/_guards/auth.guard.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return _auth_guard__WEBPACK_IMPORTED_MODULE_0__["AuthGuard"]; });




/***/ }),

/***/ "./src/app/_helpers/error.interceptor.ts":
/*!***********************************************!*\
  !*** ./src/app/_helpers/error.interceptor.ts ***!
  \***********************************************/
/*! exports provided: ErrorInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptor", function() { return ErrorInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ErrorInterceptor = /** @class */ (function () {
    function ErrorInterceptor(authenticationService) {
        this.authenticationService = authenticationService;
    }
    ErrorInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (err) {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                _this.authenticationService.logout();
                location.reload(true);
            }
            var error = err.error.message || err.statusText;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["throwError"])(error);
        }));
    };
    ErrorInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"]])
    ], ErrorInterceptor);
    return ErrorInterceptor;
}());



/***/ }),

/***/ "./src/app/_helpers/fake-backend.ts":
/*!******************************************!*\
  !*** ./src/app/_helpers/fake-backend.ts ***!
  \******************************************/
/*! exports provided: FakeBackendInterceptor, fakeBackendProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FakeBackendInterceptor", function() { return FakeBackendInterceptor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fakeBackendProvider", function() { return fakeBackendProvider; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FakeBackendInterceptor = /** @class */ (function () {
    function FakeBackendInterceptor() {
    }
    FakeBackendInterceptor.prototype.intercept = function (request, next) {
        // array in local storage for registered users
        var users = JSON.parse(localStorage.getItem('users')) || [];
        // wrap in delayed observable to simulate server api call
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(null).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["mergeMap"])(function () {
            // authenticate
            if (request.url.endsWith('/users/authenticate') && request.method === 'POST') {
                // find if any user matches login credentials
                var filteredUsers = users.filter(function (user) {
                    return user.username === request.body.username && user.password === request.body.password;
                });
                // if (filteredUsers.length) {
                //     // if login details are valid return 200 OK with user details and fake jwt token
                //     let user = filteredUsers[0];
                //     let body = {
                //         id: user.id,
                //         username: user.username,
                //         firstName: user.firstName,
                //         lastName: user.lastName,
                //         token: 'fake-jwt-token'
                //     };
                //     return of(new HttpResponse({ status: 200, body: body }));
                // } else {
                //     // else return 400 bad request
                //     return throwError({ error: { message: 'Username or password is incorrect' } });
                // }
                var user = filteredUsers[0];
                if (user.username == "demo" && user.lastName == "demo") {
                    // if login details are valid return 200 OK with user details and fake jwt token
                    var body = {
                        id: user.id,
                        username: user.username,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        token: 'fake-jwt-token'
                    };
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpResponse"]({ status: 200, body: body }));
                }
                else {
                    // else return 400 bad request
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])({ error: { message: 'Username or password is incorrect' } });
                }
            }
            // get users
            if (request.url.endsWith('/users') && request.method === 'GET') {
                // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpResponse"]({ status: 200, body: users }));
                }
                else {
                    // return 401 not authorised if token is null or invalid
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])({ status: 401, error: { message: 'Unauthorised' } });
                }
            }
            // get user by id
            if (request.url.match(/\/users\/\d+$/) && request.method === 'GET') {
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find user by id in users array
                    var urlParts = request.url.split('/');
                    var id_1 = parseInt(urlParts[urlParts.length - 1]);
                    var matchedUsers = users.filter(function (user) { return user.id === id_1; });
                    var user = matchedUsers.length ? matchedUsers[0] : null;
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpResponse"]({ status: 200, body: user }));
                }
                else {
                    // return 401 not authorised if token is null or invalid
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])({ status: 401, error: { message: 'Unauthorised' } });
                }
            }
            // register user
            if (request.url.endsWith('/users/register') && request.method === 'POST') {
                // get new user object from post body
                var newUser_1 = request.body;
                // validation
                var duplicateUser = users.filter(function (user) { return user.username === newUser_1.username; }).length;
                if (duplicateUser) {
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])({ error: { message: 'Username "' + newUser_1.username + '" is already taken' } });
                }
                // save new user
                newUser_1.id = users.length + 1;
                users.push(newUser_1);
                localStorage.setItem('users', JSON.stringify(users));
                // respond 200 OK
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpResponse"]({ status: 200 }));
            }
            // delete user
            if (request.url.match(/\/users\/\d+$/) && request.method === 'DELETE') {
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find user by id in users array
                    var urlParts = request.url.split('/');
                    var id = parseInt(urlParts[urlParts.length - 1]);
                    for (var i = 0; i < users.length; i++) {
                        var user = users[i];
                        if (user.id === id) {
                            // delete user
                            users.splice(i, 1);
                            localStorage.setItem('users', JSON.stringify(users));
                            break;
                        }
                    }
                    // respond 200 OK
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpResponse"]({ status: 200 }));
                }
                else {
                    // return 401 not authorised if token is null or invalid
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])({ status: 401, error: { message: 'Unauthorised' } });
                }
            }
            // pass through any requests not handled above
            return next.handle(request);
        }))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["materialize"])())
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(500))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["dematerialize"])());
    };
    FakeBackendInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], FakeBackendInterceptor);
    return FakeBackendInterceptor;
}());

var fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HTTP_INTERCEPTORS"],
    useClass: FakeBackendInterceptor,
    multi: true
};


/***/ }),

/***/ "./src/app/_helpers/index.ts":
/*!***********************************!*\
  !*** ./src/app/_helpers/index.ts ***!
  \***********************************/
/*! exports provided: ErrorInterceptor, JwtInterceptor, FakeBackendInterceptor, fakeBackendProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _error_interceptor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./error.interceptor */ "./src/app/_helpers/error.interceptor.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptor", function() { return _error_interceptor__WEBPACK_IMPORTED_MODULE_0__["ErrorInterceptor"]; });

/* harmony import */ var _jwt_interceptor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./jwt.interceptor */ "./src/app/_helpers/jwt.interceptor.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptor", function() { return _jwt_interceptor__WEBPACK_IMPORTED_MODULE_1__["JwtInterceptor"]; });

/* harmony import */ var _fake_backend__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fake-backend */ "./src/app/_helpers/fake-backend.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FakeBackendInterceptor", function() { return _fake_backend__WEBPACK_IMPORTED_MODULE_2__["FakeBackendInterceptor"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fakeBackendProvider", function() { return _fake_backend__WEBPACK_IMPORTED_MODULE_2__["fakeBackendProvider"]; });






/***/ }),

/***/ "./src/app/_helpers/jwt.interceptor.ts":
/*!*********************************************!*\
  !*** ./src/app/_helpers/jwt.interceptor.ts ***!
  \*********************************************/
/*! exports provided: JwtInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptor", function() { return JwtInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var JwtInterceptor = /** @class */ (function () {
    function JwtInterceptor() {
    }
    JwtInterceptor.prototype.intercept = function (request, next) {
        // add authorization header with jwt token if available
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + currentUser.token
                }
            });
        }
        return next.handle(request);
    };
    JwtInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], JwtInterceptor);
    return JwtInterceptor;
}());



/***/ }),

/***/ "./src/app/_models/deviceInfo.ts":
/*!***************************************!*\
  !*** ./src/app/_models/deviceInfo.ts ***!
  \***************************************/
/*! exports provided: DeviceInfo, DataMapperInfo, CreateFilesReq */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceInfo", function() { return DeviceInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataMapperInfo", function() { return DataMapperInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateFilesReq", function() { return CreateFilesReq; });
var DeviceInfo = /** @class */ (function () {
    function DeviceInfo() {
    }
    return DeviceInfo;
}());

var DataMapperInfo = /** @class */ (function () {
    function DataMapperInfo() {
    }
    return DataMapperInfo;
}());

var CreateFilesReq = /** @class */ (function () {
    function CreateFilesReq() {
        this.deviceInfoList = new Array();
        this.dataMapperInfoList = new Array();
    }
    return CreateFilesReq;
}());



/***/ }),

/***/ "./src/app/_models/deviceInfoList.ts":
/*!*******************************************!*\
  !*** ./src/app/_models/deviceInfoList.ts ***!
  \*******************************************/
/*! exports provided: DeviceInfoList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceInfoList", function() { return DeviceInfoList; });
var DeviceInfoList = /** @class */ (function () {
    function DeviceInfoList() {
        this.deviceInfoList = [];
        this.deviceInfoList = new Array();
    }
    return DeviceInfoList;
}());



/***/ }),

/***/ "./src/app/_models/index.ts":
/*!**********************************!*\
  !*** ./src/app/_models/index.ts ***!
  \**********************************/
/*! exports provided: User, DeviceInfo, DataMapperInfo, CreateFilesReq, DeviceInfoList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user */ "./src/app/_models/user.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "User", function() { return _user__WEBPACK_IMPORTED_MODULE_0__["User"]; });

/* harmony import */ var _deviceInfo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./deviceInfo */ "./src/app/_models/deviceInfo.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DeviceInfo", function() { return _deviceInfo__WEBPACK_IMPORTED_MODULE_1__["DeviceInfo"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DataMapperInfo", function() { return _deviceInfo__WEBPACK_IMPORTED_MODULE_1__["DataMapperInfo"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "CreateFilesReq", function() { return _deviceInfo__WEBPACK_IMPORTED_MODULE_1__["CreateFilesReq"]; });

/* harmony import */ var _deviceInfoList__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./deviceInfoList */ "./src/app/_models/deviceInfoList.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DeviceInfoList", function() { return _deviceInfoList__WEBPACK_IMPORTED_MODULE_2__["DeviceInfoList"]; });






/***/ }),

/***/ "./src/app/_models/user.ts":
/*!*********************************!*\
  !*** ./src/app/_models/user.ts ***!
  \*********************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./src/app/_services/alert.service.ts":
/*!********************************************!*\
  !*** ./src/app/_services/alert.service.ts ***!
  \********************************************/
/*! exports provided: AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return AlertService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AlertService = /** @class */ (function () {
    function AlertService(router) {
        var _this = this;
        this.router = router;
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.keepAfterNavigationChange = false;
        // clear alert message on route change
        router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationStart"]) {
                if (_this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    _this.keepAfterNavigationChange = false;
                }
                else {
                    // clear alert
                    _this.subject.next();
                }
            }
        });
    }
    AlertService.prototype.success = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        console.log("Success, I am in alert");
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message });
    };
    AlertService.prototype.error = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'error', text: message });
    };
    AlertService.prototype.getMessage = function () {
        return this.subject.asObservable();
    };
    AlertService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AlertService);
    return AlertService;
}());



/***/ }),

/***/ "./src/app/_services/authentication.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/_services/authentication.service.ts ***!
  \*****************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http) {
        this.http = http;
        //private AUTH_TOKEN_URL:string = "http://localhost:8082";
        this.AUTH_TOKEN_URL = "https://10.111.84.198:8383";
        // private EDA_URL:string = "http://10.111.84.198:8080";
        //private EDA_URL:string = "http://localhost:8082";
        this.userData = {
            "client_id": "EDA_M2M_PASSWORD_GRANT_CLIENT_b47b731a-62e0-4f12-9421-033758c2c0f7",
            "client_secret": "7544c077-a088-416a-83a6-71e3d26082f8",
            "grant_type": "password",
            "username": "admin",
            "password": "Admin@123456",
            "scope": "scopes.ericsson.com/activation/resource_configuration.device_inventory.device.config.read scopes.ericsson.com/activation/resource_configuration.device_inventory.device.read openid"
        };
    }
    AuthenticationService.prototype.login = function (username, password) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl + "/users/authenticate", { username: username, password: password })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (user) {
            // login successful if there's a jwt token in the response
            if (user && user.token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                localStorage.setItem('isLoggedIn', "Yes");
            }
            return user;
        }));
    };
    AuthenticationService.prototype.logout = function () {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    };
    AuthenticationService.prototype.authToken = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this.http.post(this.AUTH_TOKEN_URL + '/oauth/v1/token', this.userData, { headers: headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (token) {
            localStorage.setItem('accessToekn', '');
            return token;
        }));
    };
    AuthenticationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthenticationService);
    return AuthenticationService;
}());



/***/ }),

/***/ "./src/app/_services/eda.service.ts":
/*!******************************************!*\
  !*** ./src/app/_services/eda.service.ts ***!
  \******************************************/
/*! exports provided: EDAService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EDAService", function() { return EDAService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EDAService = /** @class */ (function () {
    function EDAService(http) {
        this.http = http;
        // private EDA_URL:string = "http://localhost:8082";
        this.EDA_URL = "http://10.111.84.198:1248";
    }
    EDAService.prototype.deviceList = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        headers.append("Authorization", "bearer " + localStorage.getItem('accessToekn'));
        return this.http.get(this.EDA_URL + '/scm-rest/device-repository/devices?pageSize=50&pageIndex=1', { headers: headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (resp) {
            console.log("I am in devicelist response");
            return resp;
        }));
    };
    EDAService.prototype.attributeList = function (deviceId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        headers.append("Authorization", "bearer " + localStorage.getItem('accessToekn'));
        return this.http.get(this.EDA_URL + '/scm-rest/device-repository/device/config?deviceId=' + deviceId, { headers: headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (resp) {
            return resp;
        }));
    };
    EDAService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], EDAService);
    return EDAService;
}());



/***/ }),

/***/ "./src/app/_services/filecreation.service.ts":
/*!***************************************************!*\
  !*** ./src/app/_services/filecreation.service.ts ***!
  \***************************************************/
/*! exports provided: FileCreationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileCreationService", function() { return FileCreationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FileCreationService = /** @class */ (function () {
    function FileCreationService(http) {
        this.http = http;
        //private EDA_TOKEN_URL:string = "http://localhost:8082;   
        this.EDA_URL = "http://10.111.84.198:1248";
        // private EDA_URL:string = "http://localhost:8080";
        //private CAMUNDA_URL:string = "http://10.111.84.197:8080";
        this.CAMUNDA_URL = "http://localhost:8080";
    }
    FileCreationService.prototype.createFiles = function (req) {
        return this.http.post(this.EDA_URL + "/eda-api/so/services/files", req)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (resp) {
            return resp;
        }));
    };
    FileCreationService.prototype.runScript = function () {
        return this.http.post(this.EDA_URL + "/eda-api/so/services/script/run", {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (resp) {
            return resp;
        }));
    };
    FileCreationService.prototype.startwf1 = function () {
        return this.http.post(this.CAMUNDA_URL + "/att-demo/so/services/att-instantiate-uc1", {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (resp) {
            return resp;
        }));
    };
    FileCreationService.prototype.startwf2 = function () {
        return this.http.post(this.CAMUNDA_URL + "/att-demo/so/services/att-instantiate-uc1", {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (resp) {
            return resp;
        }));
    };
    FileCreationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], FileCreationService);
    return FileCreationService;
}());



/***/ }),

/***/ "./src/app/_services/index.ts":
/*!************************************!*\
  !*** ./src/app/_services/index.ts ***!
  \************************************/
/*! exports provided: AlertService, AuthenticationService, UserService, FileCreationService, EDAService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _alert_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./alert.service */ "./src/app/_services/alert.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return _alert_service__WEBPACK_IMPORTED_MODULE_0__["AlertService"]; });

/* harmony import */ var _authentication_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./authentication.service */ "./src/app/_services/authentication.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return _authentication_service__WEBPACK_IMPORTED_MODULE_1__["AuthenticationService"]; });

/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user.service */ "./src/app/_services/user.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return _user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]; });

/* harmony import */ var _filecreation_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./filecreation.service */ "./src/app/_services/filecreation.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FileCreationService", function() { return _filecreation_service__WEBPACK_IMPORTED_MODULE_3__["FileCreationService"]; });

/* harmony import */ var _eda_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./eda.service */ "./src/app/_services/eda.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EDAService", function() { return _eda_service__WEBPACK_IMPORTED_MODULE_4__["EDAService"]; });








/***/ }),

/***/ "./src/app/_services/user.service.ts":
/*!*******************************************!*\
  !*** ./src/app/_services/user.service.ts ***!
  \*******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService.prototype.getAll = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + "/users");
    };
    UserService.prototype.getById = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + "/users/" + id);
    };
    UserService.prototype.register = function (user) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + "/users/register", user);
    };
    UserService.prototype.update = function (user) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + "/users/" + user.id, user);
    };
    UserService.prototype.delete = function (id) {
        return this.http.delete(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + "/users/" + id);
    };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".jumbotron[_ngcontent-c0] {\r\n    padding: 6rem 0rem;\r\n    margin-bottom: 0rem;\r\n    background-color: white;\r\n    border-radius: .2rem;\r\n    margin-left: 7rem;\r\n    margin-right: 3rem;\r\n    margin-top: 3rem;\r\n}\r\n\r\n.row {\r\n    display: flex;\r\n    align-self: center;\r\n    flex-wrap: wrap;\r\n    margin-right: 5px;\r\n    margin-left: 5px;\r\n    width:100%;\r\n}\r\n\r\n.row-centered { align-content: center; margin: auto; max-width: 300px;}\r\n\r\n.form-control {\r\n    display: block;\r\n    width: 30%;\r\n    padding: .375rem .75rem;\r\n    font-size: 1rem;\r\n    line-height: 1.5;\r\n    color: #495057;\r\n    background-color: #fff;\r\n    background-clip: padding-box;\r\n    border: 1px solid #ced4da;\r\n    border-radius: .25rem;\r\n    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;\r\n}\r\n\r\n* {\r\n    margin: 0; padding: 0;\r\n}\r\n\r\nhtml, body, #container {\r\n    height: 100%;\r\n    width: 90%;\r\n}\r\n\r\nheader {\r\n    height: 30px;\r\n    background: gray;\r\n}\r\n\r\nfooter {\r\n    height: 30px;\r\n    background: gray;\r\n}\r\n\r\nmain {\r\n    height: 90%;\r\n}\r\n\r\n.half {\r\n    height: 90%;\r\n}\r\n\r\n.half:first-child {\r\n    background: white;\r\n}\r\n\r\n.half:last-child {\r\n    background: yellow;\r\n}\r\n\r\n/* *[_ngcontent-c0] {\r\n    margin: 0px;\r\n    padding: 25px;\r\n} */\r\n\r\n.mat-form-field-appearance-legacy .mat-form-field-infix {\r\n    padding: .4375em 0;\r\n}\r\n\r\n.mat-form-field-infix {\r\n    display: block;\r\n    position: relative;\r\n    flex: auto;\r\n    min-width: 0;\r\n    width: 150px;\r\n}"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<!-- <app-sidebar></app-sidebar> -->\n<!-- main app container -->\n<!-- <header align=\"center\">\n    Welcome to Ericsson Demo project!! \n    <a [routerLink]=\"['/login']\">Logout</a>\n</header> -->\n<!-- <p align=\"center\">Welcome to Ericsson Demo project!! \n    <a [routerLink]=\"['/login']\">Logout</a>\n</p> -->\n<div class=\"jumbotron\">\n    <div class=\"container\">\n        <!-- <div [ngStyle]=\"{'background' : 'url(./assets/images/bk_image.jpg)'}\">          -->\n        <div>\n            <alert></alert>  \n            <router-outlet></router-outlet>\n        </div>\n    </div>\n</div>\n\n<!-- <footer>\n<table border=\"0\" align=\"center\" width=\"100%\">\n    <tr>\n        <td align=\"left\">\n            Here are some links to help you start:\n        </td>\n        <td align=\"center\">\n            <a target=\"_blank\" rel=\"noopener\" href=\"https://www.ericsson.com\">Ericsson</a>\n        </td>\n        <td align=\"right\">\n            <a target=\"_blank\" rel=\"noopener\" href=\"https://www.att.com\">AT&T</a>\n        </td>\n    </tr>\n</table>\n</footer> -->"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./_models */ "./src/app/_models/index.ts");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./_services */ "./src/app/_services/index.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppComponent = /** @class */ (function () {
    function AppComponent(route, router, userService) {
        this.route = route;
        this.router = router;
        this.userService = userService;
        this.loading = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        var user = new _models__WEBPACK_IMPORTED_MODULE_2__["User"]();
        user.username = "demo";
        user.password = "demo";
        user.firstName = "demo";
        user.lastName = "demo";
        this.loading = true;
        this.userService.register(user)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["first"])())
            .subscribe(function (data) {
            _this.router.navigate(['/login']);
        }, function (error) {
            _this.loading = false;
        });
        localStorage.setItem('isLoggedIn', "No");
        this.router.navigate(['/login']);
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services__WEBPACK_IMPORTED_MODULE_3__["UserService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_tabs_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-tabs-component */ "./node_modules/angular-tabs-component/dist/index.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/sidebar/sidebar.component */ "./src/app/components/sidebar/sidebar.component.ts");
/* harmony import */ var _components_header_header_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/header/header.component */ "./src/app/components/header/header.component.ts");
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./_helpers */ "./src/app/_helpers/index.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var _directives__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./_directives */ "./src/app/_directives/index.ts");
/* harmony import */ var _guards__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./_guards */ "./src/app/_guards/index.ts");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./_services */ "./src/app/_services/index.ts");
/* harmony import */ var _welcome__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./welcome */ "./src/app/welcome/index.ts");
/* harmony import */ var _home__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./home */ "./src/app/home/index.ts");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./config */ "./src/app/config/index.ts");
/* harmony import */ var _login__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./login */ "./src/app/login/index.ts");
/* harmony import */ var _register__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./register */ "./src/app/register/index.ts");
/* harmony import */ var _workflow__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./workflow */ "./src/app/workflow/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










//import { TranslateModule } from '@ngx-translate/core';



//import { TranslateService, TranslateStore, TranslateLoader, 
//  TranslateCompiler, TranslateParser } from '@ngx-translate/core';
// used to create fake backend














var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
                angular_tabs_component__WEBPACK_IMPORTED_MODULE_3__["TabModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_4__["MatExpansionModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_8__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"],
                //MdSelectModule,
                //MaterialModule,
                //TranslateModule,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_14__["routing"]
            ],
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_13__["AppComponent"],
                _directives__WEBPACK_IMPORTED_MODULE_15__["AlertComponent"],
                _home__WEBPACK_IMPORTED_MODULE_19__["HomeComponent"],
                _config__WEBPACK_IMPORTED_MODULE_20__["ConfigComponent"],
                _login__WEBPACK_IMPORTED_MODULE_21__["LoginComponent"],
                _register__WEBPACK_IMPORTED_MODULE_22__["RegisterComponent"],
                _welcome__WEBPACK_IMPORTED_MODULE_18__["WelcomeComponent"],
                _workflow__WEBPACK_IMPORTED_MODULE_23__["WorkflowComponent"],
                _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_10__["SidebarComponent"],
                _components_header_header_component__WEBPACK_IMPORTED_MODULE_11__["HeaderComponent"]
            ],
            providers: [
                _guards__WEBPACK_IMPORTED_MODULE_16__["AuthGuard"],
                _services__WEBPACK_IMPORTED_MODULE_17__["AlertService"],
                _services__WEBPACK_IMPORTED_MODULE_17__["AuthenticationService"],
                _services__WEBPACK_IMPORTED_MODULE_17__["UserService"],
                _services__WEBPACK_IMPORTED_MODULE_17__["EDAService"],
                _services__WEBPACK_IMPORTED_MODULE_17__["FileCreationService"],
                //   TranslateService,
                //   TranslateStore,        
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HTTP_INTERCEPTORS"], useClass: _helpers__WEBPACK_IMPORTED_MODULE_12__["JwtInterceptor"], multi: true },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HTTP_INTERCEPTORS"], useClass: _helpers__WEBPACK_IMPORTED_MODULE_12__["ErrorInterceptor"], multi: true },
                // provider used to create fake backend
                _helpers__WEBPACK_IMPORTED_MODULE_12__["fakeBackendProvider"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_13__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: routing */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routing", function() { return routing; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home */ "./src/app/home/index.ts");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./config */ "./src/app/config/index.ts");
/* harmony import */ var _login__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login */ "./src/app/login/index.ts");
/* harmony import */ var _register__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./register */ "./src/app/register/index.ts");
/* harmony import */ var _workflow__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./workflow */ "./src/app/workflow/index.ts");
/* harmony import */ var _guards__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./_guards */ "./src/app/_guards/index.ts");







var appRoutes = [
    { path: '', component: _login__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'home', component: _home__WEBPACK_IMPORTED_MODULE_1__["HomeComponent"], canActivate: [_guards__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]] },
    { path: 'login', component: _login__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'workflow', component: _workflow__WEBPACK_IMPORTED_MODULE_5__["WorkflowComponent"] },
    { path: 'config', component: _config__WEBPACK_IMPORTED_MODULE_2__["ConfigComponent"] },
    { path: 'register', component: _register__WEBPACK_IMPORTED_MODULE_4__["RegisterComponent"] },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];
var routing = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(appRoutes);


/***/ }),

/***/ "./src/app/components/header/header.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg fixed-top\">\n    <a class=\"navbar-brand\" href=\"javascript:void(0)\"><img class=\"pull-left\"\n\t\t\t\tstyle=\"margin: 5px 5px 0 0;\" src=\"./assets/images/ericsson_logo.png\" width=\"20px\">Ericsson Network Service Orchestrator</a>\n    <button class=\"navbar-toggler\" type=\"button\" (click)=\"toggleSidebar()\">\n        <i class=\"fa fa-bars text-muted\" aria-hidden=\"true\"></i>\n    </button>\n    <!-- <div *ngIf=\"isLoggedIn()\"> -->\n        <div *ngIf=\"isLoggedIn()\" class=\"dropdown-menu-right\" ngbDropdownMenu>\n            <!-- <a [routerLink]=\"['/home']\">\n                <span class=\"badge badge-info\"> {{ 'Home' }}</span>\n            </a>  -->\n            <div class=\"divider\"></div>\n            <a [routerLink]=\"['/config']\">\n                <span class=\"badge badge-info\"> {{ 'Device Mapping' }}</span>\n            </a>\n            <div class=\"divider\"></div>\n            <a [routerLink]=\"['/workflow']\">\n                <span class=\"badge badge-info\"> {{ 'Camunda Workflow' }}</span>\n            </a>\n        </div>\n        <div *ngIf=\"isLoggedIn()\" class=\"collapse navbar-collapse\">\n            <ul class=\"navbar-nav ml-auto\">            \n                <li class=\"nav-item dropdown\" ngbDropdown>\n                    <a href=\"javascript:void(0)\" class=\"nav-link\" ngbDropdownToggle>\n                        <i class=\"fa fa-bell\"></i> <b class=\"caret\"></b><span class=\"sr-only\">(current)</span>\n                    </a>                \n                    <div class=\"dropdown-menu-right\" ngbDropdownMenu>\n                        <a [routerLink]=\"['/login']\">\n                            <span class=\"badge badge-info\" (click)=\"onLoggedout()\"> {{ 'Log Out' }}</span>\n                        </a>                    \n                    </div>\n                </li>\n       \n            <!-- <li class=\"nav-item dropdown\" ngbDropdown>\n                <a href=\"javascript:void(0)\" class=\"nav-link\" ngbDropdownToggle>\n                    <i class=\"fa fa-user\"></i> {{ getCurrentUser() }} <b class=\"caret\"></b>\n                </a>\n                <div class=\"dropdown-menu-right\" ngbDropdownMenu>\n                    <a [routerLink]=\"['/info']\" [routerLinkActive]=\"['router-link-active']\" class=\"dropdown-item\">\n                        <i class=\"fa fa-fw fa-gear\"></i> {{ 'Information' }}\n                    </a>\n                    <a class=\"dropdown-item\" [routerLink]=\"['/login']\" (click)=\"onLoggedout()\">\n                        <i class=\"fa fa-fw fa-power-off\"></i> {{ 'Log Out' }}\n                    </a>\n                </div>\n            </li> -->\n            </ul>\n        <!-- </div> -->\n    </div>\n</nav>"

/***/ }),

/***/ "./src/app/components/header/header.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .navbar {\n  background-color: #222; }\n  :host .navbar .navbar-brand {\n    color: #fff; }\n  :host .navbar .nav-item > a {\n    color: #999; }\n  :host .navbar .nav-item > a:hover {\n      color: #fff; }\n  :host .messages {\n  width: 200px; }\n  :host .messages .media {\n    border-bottom: 1px solid #ddd;\n    padding: 5px 10px; }\n  :host .messages .media:last-child {\n      border-bottom: none; }\n  :host .messages .media-body h5 {\n    font-size: 13px;\n    font-weight: 600; }\n  :host .messages .media-body .small {\n    margin: 0; }\n  :host .messages .media-body .last {\n    font-size: 12px;\n    margin: 0; }\n  .badge {\n  display: inline-block;\n  font-size: 80%;\n  font-weight: 700;\n  line-height: 2;\n  text-align: center;\n  white-space: nowrap;\n  vertical-align: baseline;\n  border-radius: .5rem; }\n  .divider {\n  width: 10px;\n  height: auto;\n  display: inline-block; }\n"

/***/ }),

/***/ "./src/app/components/header/header.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { TranslateService } from '@ngx-translate/core';
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router) {
        // this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        // this.translate.setDefaultLang('en');
        // const browserLang = this.translate.getBrowserLang();
        // this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
        var _this = this;
        this.router = router;
        this.pushRightClass = 'push-right';
        this.router.events.subscribe(function (val) {
            if (val instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"] &&
                window.innerWidth <= 992 &&
                _this.isToggled()) {
                _this.toggleSidebar();
            }
        });
    }
    HeaderComponent.prototype.ngOnInit = function () { };
    HeaderComponent.prototype.isToggled = function () {
        var dom = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    };
    HeaderComponent.prototype.toggleSidebar = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    };
    HeaderComponent.prototype.rltAndLtr = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle('rtl');
    };
    HeaderComponent.prototype.onLoggedout = function () {
        localStorage.removeItem('isLoggedin');
        localStorage.setItem('isLoggedIn', "No");
    };
    HeaderComponent.prototype.isLoggedIn = function () {
        var isLoggedIn = localStorage.getItem('isLoggedIn');
        if (isLoggedIn == "Yes") {
            return true;
        }
        return false;
    };
    HeaderComponent.prototype.changeLang = function (language) {
        // this.translate.use(language);
    };
    HeaderComponent.prototype.getCurrentUser = function () {
        return localStorage.getItem('username');
    };
    HeaderComponent.prototype.getTotalSDPTaskCount = function () {
        var cn = Number(localStorage.getItem('dashSDPTasks'));
        var cn2 = Number(localStorage.getItem('dashSDPCTasks'));
        console.log("SDP ->", cn + cn2);
        return cn + cn2;
    };
    HeaderComponent.prototype.getTotalOCCTaskCount = function () {
        var cn = Number(localStorage.getItem('dashOCCTasks'));
        console.log("OCC ->", cn);
        return cn;
    };
    HeaderComponent.prototype.getTotalAIRTaskCount = function () {
        var cn = Number(localStorage.getItem('dashAIRTasks'));
        console.log("AIR ->", cn);
        return cn;
    };
    HeaderComponent.prototype.getTotalEMMTaskCount = function () {
        var cn = Number(localStorage.getItem('dashEMMTasks'));
        console.log("EMM ->", cn);
        return cn;
    };
    HeaderComponent.prototype.getTotalStatusCount = function () {
        var sdp = Number(localStorage.getItem('dashSDPStatus'));
        var occ = Number(localStorage.getItem('dashOCCStatus'));
        var air = Number(localStorage.getItem('dashAIRStatus'));
        var cnt = sdp + occ + air;
        return cnt;
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/components/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/sidebar/sidebar.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/sidebar/sidebar.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"sidebar\" [ngClass]=\"{sidebarPushRight: isActive}\">\n    <div class=\"list-group\">\n        <a routerLink=\"/home\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n            <i class=\"fa fa-fw fa-dashboard\"></i>&nbsp;{{ 'Home' }}\n        </a>\n        <!-- NOT NEEDED RIGHT NOW\n        <a [routerLink]=\"['/charts']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n            <i class=\"fa fa-fw fa-bar-chart-o\"></i>&nbsp;{{ 'Charts' | translate }}\n        </a>\n        <a [routerLink]=\"['/tables']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n            <i class=\"fa fa-fw fa-table\"></i>&nbsp;{{ 'Tables' | translate }}\n        </a>\n        <a [routerLink]=\"['/forms']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n            <i class=\"fa fa-fw fa-edit\"></i>&nbsp;{{ 'Forms' | translate }}\n        </a>\n        <a [routerLink]=\"['/bs-element']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n            <i class=\"fa fa-fw fa-desktop\"></i>&nbsp;{{ 'Bootstrap Element' | translate }}\n        </a>\n        <a [routerLink]=\"['/grid']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n            <i class=\"fa fa-fw fa-wrench\"></i>&nbsp;{{ 'Bootstrap Grid' | translate }}\n        </a>\n        <a [routerLink]=\"['/components']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n            <i class=\"fa fa-th-list\"></i>&nbsp;{{ 'Component' | translate }}\n        </a>\n        -->\n        <div class=\"nested-menu\">\n            <a class=\"list-group-item\" (click)=\"addExpandClass('pages')\">\n                <div *ngIf=\"!isExpanded\">\n                    <span><i class=\"fa fa-plus\"></i>&nbsp; {{ 'Home' }}</span>\n                </div>\n                <div *ngIf=\"isExpanded\">\n                    <span><i class=\"fa fa-minus\"></i>&nbsp; {{ 'Home' }}</span>\n                </div>\n\n            </a>\n            <li class=\"nested\" [class.expand]=\"showMenu === 'pages'\">\n                <ul class=\"submenu\">\n                    <li>\n                        <a [routerLink]=\"['/home']\" [routerLinkActive]=\"['router-link-active']\"><span>{{ 'Home' }}</span></a>\n                    </li>\n                    <!-- <li>\n                        <a [routerLink]=\"['/occ']\" [routerLinkActive]=\"['router-link-active']\"><span>{{ 'OCC'  | translate }}</span></a>\n                    </li>\n                    <li>\n                        <a [routerLink]=\"['/air']\" [routerLinkActive]=\"['router-link-active']\"><span>{{ 'AIR' | translate }}</span></a>\n                    </li>\n                    <li>\n                        <a [routerLink]=\"['/emm']\" [routerLinkActive]=\"['router-link-active']\"><span>{{ 'EMM' | translate }}</span></a>\n                    </li> -->\n                    <!--\n                    <li>\n                        <a href=\"javascript:void(0)\"><span>{{ 'EMM' | translate }}</span></a>\n                    </li>\n                -->\n                </ul>\n            </li>\n        </div>\n        <a [routerLink]=\"['/home']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n            <i class=\"fa fa-fw fa-edit\"></i>&nbsp;{{ 'Home '}\n        </a>\n        <!-- <a [routerLink]=\"['/migration']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n            <i class=\"fa fa-fw fa-chevron-circle-up\"></i>&nbsp;{{ 'Migration' | translate }}\n        </a>\n        <a [routerLink]=\"['/info']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n            <i class=\"fa fa-fw fa-info\"></i>&nbsp;{{ 'Information' | translate }}\n        </a> -->\n        <!-- NOT NEEDED\n        <a [routerLink]=\"['/blank-page']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n            <i class=\"fa fa-file-o\"></i>&nbsp;{{ 'Blank Page' | translate }}\n        </a>\n        <a class=\"list-group-item more-themes\" href=\"http://www.strapui.com/\">\n            {{ 'More Theme' | translate }}\n        </a>\n      -->\n        <div class=\"header-fields\">\n            <a (click)=\"rltAndLtr()\" class=\"list-group-item\">\n                <span><i class=\"fa fa-arrows-h\"></i>&nbsp; RTL/LTR</span>\n            </a>\n            <div class=\"nested-menu\">\n                <a class=\"list-group-item\" (click)=\"addExpandClass('languages')\">\n                    <span><i class=\"fa fa-language\"></i>&nbsp; {{ 'Language' } <b class=\"caret\"></b></span>\n                </a>\n                <li class=\"nested\" [class.expand]=\"showMenu === 'languages'\">\n                    <ul class=\"submenu\">\n                        <li>\n                            <a href=\"javascript:void(0)\" (click)=\"changeLang('en')\">\n                                {{ 'English' }}\n                            </a>\n                        </li>\n                        <li>\n                            <a href=\"javascript:void(0)\" (click)=\"changeLang('fr')\">\n                                {{ 'French' }}\n                            </a>\n                        </li>\n                        <li>\n                            <a href=\"javascript:void(0)\" (click)=\"changeLang('ur')\">\n                                {{ 'Urdu' }}\n                            </a>\n                        </li>\n                        <li>\n                            <a href=\"javascript:void(0)\" (click)=\"changeLang('es')\">\n                                {{ 'Spanish' }}\n                            </a>\n                        </li>\n                        <li>\n                            <a href=\"javascript:void(0)\" (click)=\"changeLang('it')\">\n                                {{ 'Italian' }}\n                            </a>\n                        </li>\n                        <li>\n                            <a href=\"javascript:void(0)\" (click)=\"changeLang('fa')\">\n                                {{ 'Farsi' }}\n                            </a>\n                        </li>\n                        <li>\n                            <a href=\"javascript:void(0)\" (click)=\"changeLang('de')\">\n                                {{ 'German' }}\n                            </a>\n                        </li>\n                    </ul>\n                </li>\n            </div>\n            <!-- <div class=\"nested-menu\">\n                <a class=\"list-group-item\" (click)=\"addExpandClass('profile')\">\n                    <span><i class=\"fa fa-user\"></i>&nbsp; John Smith</span>\n                </a>\n                <li class=\"nested\" [class.expand]=\"showMenu === 'profile'\">\n                    <ul class=\"submenu\">\n                        <li>\n                            <a href=\"javascript:void(0)\">\n                                <span><i class=\"fa fa-fw fa-user\"></i> {{ 'Profile' | translate }}</span>\n                            </a>\n                        </li>\n                        <li>\n                            <a href=\"javascript:void(0)\">\n                                <span><i class=\"fa fa-fw fa-envelope\"></i> {{ 'Inbox' | translate }}</span>\n                            </a>\n                        </li>\n                        <li>\n                            <a href=\"javascript:void(0)\">\n                                <span><i class=\"fa fa-fw fa-gear\"></i> {{ 'Settings' | translate }}</span>\n                            </a>\n                        </li>\n                        <li>\n                            <a [routerLink]=\"['/login']\" (click)=\"onLoggedout()\">\n                                <span><i class=\"fa fa-fw fa-power-off\"></i> {{ 'Log Out' | translate }}</span>\n                            </a>\n                        </li>\n                    </ul>\n                </li>\n            </div> -->\n        </div>\n    </div>\n</nav>"

/***/ }),

/***/ "./src/app/components/sidebar/sidebar.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/components/sidebar/sidebar.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidebar {\n  border-radius: 0;\n  position: fixed;\n  z-index: 1000;\n  top: 56px;\n  left: 235px;\n  width: 235px;\n  margin-left: -235px;\n  border: none;\n  border-radius: 0;\n  overflow-y: auto;\n  background-color: #222;\n  bottom: 0;\n  overflow-x: hidden;\n  padding-bottom: 40px;\n  transition: all 0.2s ease-in-out; }\n  .sidebar .list-group a.list-group-item {\n    background: #222;\n    border: 0;\n    border-radius: 0;\n    color: #999;\n    text-decoration: none; }\n  .sidebar .list-group a.list-group-item .fa {\n      margin-right: 10px; }\n  .sidebar .list-group a:hover {\n    background: #151515;\n    color: #fff; }\n  .sidebar .list-group a.router-link-active {\n    background: #151515;\n    color: #fff; }\n  .sidebar .list-group .header-fields {\n    padding-top: 10px; }\n  .sidebar .list-group .header-fields > .list-group-item:first-child {\n      border-top: 1px solid rgba(255, 255, 255, 0.2); }\n  .sidebar .sidebar-dropdown *:focus {\n    border-radius: none;\n    border: none; }\n  .sidebar .sidebar-dropdown .panel-title {\n    font-size: 1rem;\n    height: 50px;\n    margin-bottom: 0; }\n  .sidebar .sidebar-dropdown .panel-title a {\n      color: #999;\n      text-decoration: none;\n      font-weight: 400;\n      background: #222; }\n  .sidebar .sidebar-dropdown .panel-title a span {\n        position: relative;\n        display: block;\n        padding: 0.75rem 1.5rem;\n        padding-top: 1rem; }\n  .sidebar .sidebar-dropdown .panel-title a:hover,\n    .sidebar .sidebar-dropdown .panel-title a:focus {\n      color: #fff;\n      outline: none;\n      outline-offset: -2px; }\n  .sidebar .sidebar-dropdown .panel-title:hover {\n    background: #151515; }\n  .sidebar .sidebar-dropdown .panel-collapse {\n    border-radious: 0;\n    border: none; }\n  .sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item {\n      border-radius: 0;\n      background-color: #222;\n      border: 0 solid transparent; }\n  .sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a {\n        color: #999; }\n  .sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a:hover {\n        color: #fff; }\n  .sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item:hover {\n      background: #151515; }\n  .nested-menu .list-group-item {\n  cursor: pointer; }\n  .nested-menu .nested {\n  list-style-type: none; }\n  .nested-menu ul.submenu {\n  display: none;\n  height: 0; }\n  .nested-menu .expand ul.submenu {\n  display: block;\n  list-style-type: none;\n  height: auto; }\n  .nested-menu .expand ul.submenu li a {\n    color: #fff;\n    padding: 10px;\n    display: block; }\n  @media screen and (max-width: 992px) {\n  .sidebar {\n    top: 54px;\n    left: 0px; } }\n  @media print {\n  .sidebar {\n    display: none !important; } }\n  @media (min-width: 992px) {\n  .header-fields {\n    display: none; } }\n  ::-webkit-scrollbar {\n  width: 8px; }\n  ::-webkit-scrollbar-track {\n  -webkit-box-shadow: inset 0 0 0px white;\n  border-radius: 3px; }\n  ::-webkit-scrollbar-thumb {\n  border-radius: 3px;\n  -webkit-box-shadow: inset 0 0 3px white; }\n"

/***/ }),

/***/ "./src/app/components/sidebar/sidebar.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/sidebar/sidebar.component.ts ***!
  \*********************************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { TranslateService } from '@ngx-translate/core';
var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(router) {
        // this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        // this.translate.setDefaultLang('en');
        // const browserLang = this.translate.getBrowserLang();
        // this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');
        var _this = this;
        this.router = router;
        this.isActive = false;
        this.showMenu = '';
        this.pushRightClass = 'push-right';
        this.isExpanded = false;
        this.router.events.subscribe(function (val) {
            if (val instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"] &&
                window.innerWidth <= 992 &&
                _this.isToggled()) {
                _this.toggleSidebar();
            }
        });
    }
    SidebarComponent.prototype.eventCalled = function () {
        this.isActive = !this.isActive;
    };
    SidebarComponent.prototype.addExpandClass = function (element) {
        if (element === this.showMenu) {
            this.showMenu = '0';
            this.isExpanded = false;
        }
        else {
            this.showMenu = element;
            this.isExpanded = true;
        }
    };
    SidebarComponent.prototype.isToggled = function () {
        var dom = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    };
    SidebarComponent.prototype.toggleSidebar = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    };
    SidebarComponent.prototype.rltAndLtr = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle('rtl');
    };
    SidebarComponent.prototype.changeLang = function (language) {
        // this.translate.use(language);
    };
    SidebarComponent.prototype.onLoggedout = function () {
        localStorage.removeItem('isLoggedin');
    };
    SidebarComponent.prototype.getNumberOfSDPTasks = function () {
        return 4;
    };
    SidebarComponent.prototype.getNumberOfOCCTasks = function () {
        return 5;
    };
    SidebarComponent.prototype.getNumberOfAirTasks = function () {
        return 6;
    };
    SidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar',
            template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/components/sidebar/sidebar.component.html"),
            styles: [__webpack_require__(/*! ./sidebar.component.scss */ "./src/app/components/sidebar/sidebar.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/config/config.component.css":
/*!*********************************************!*\
  !*** ./src/app/config/config.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-headers-align .mat-expansion-panel-header-title,\r\n.example-headers-align .mat-expansion-panel-header-description {\r\n  flex-basis: 0;\r\n}\r\n\r\n.example-headers-align .mat-expansion-panel-header-description {\r\n  justify-content: space-between;\r\n  align-items: center;\r\n}\r\n\r\n.divider{\r\n  width:5px;\r\n  height:auto;\r\n  display:inline-block;\r\n}\r\n\r\n.mat-expansion-panel-content {\r\n  font: 400 12px/20px Roboto,\"Helvetica Neue\",sans-serif;\r\n}\r\n\r\n.mat-form-field-appearance-legacy .mat-form-field-infix {\r\n  padding: .4375em 0;\r\n}\r\n\r\n.mat-form-field-appearance-legacy .mat-form-field-infix {\r\n  padding: .4375em 0;\r\n}\r\n\r\n.mat-form-field-infix {\r\n  display: block;\r\n  position: relative;\r\n  flex: auto;\r\n  min-width: 0;\r\n  width: 150px;\r\n}\r\n\r\n.btn_sub {\r\n  display: inline-block;\r\n  font-weight: 400;\r\n  text-align: center;\r\n  white-space: nowrap;\r\n  vertical-align: middle;\r\n  -webkit-user-select: none;\r\n  -moz-user-select: none;\r\n  -ms-user-select: none;\r\n  user-select: none;\r\n  border: 1px solid transparent;\r\n  padding: .375rem .75rem;\r\n  font-size: 1rem;\r\n  line-height: 1.5;\r\n  float: right;\r\n  border-radius: .25rem;\r\n  transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;\r\n}\r\n\r\n/* .btn-primary {\r\n  color: #fff;\r\n  background-color: gray;\r\n  border-color: #007bff;\r\n} */"

/***/ }),

/***/ "./src/app/config/config.component.html":
/*!**********************************************!*\
  !*** ./src/app/config/config.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"homeForm\" width=\"100%\" border=\"1\">\n<mat-accordion class=\"example-headers-align\">\n    <mat-expansion-panel [expanded]=\"step === 0\" (opened)=\"setStep(0)\" hideToggle>\n      <mat-expansion-panel-header align=\"center\">\n        <mat-panel-title>\n          <b>Device Mapping</b>\n        </mat-panel-title>        \n      </mat-expansion-panel-header>\n      \n        &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;\n        <mat-form-field>  \n            <mat-select placeholder=\"Select Source Device Id\" [(value)]=\"selectedSourceDeviceId\">\n              <mat-option *ngFor=\"let option of dropdown1\"\n              value=\"{{ option }}\"> {{ option }}</mat-option>\n            </mat-select>\n          <!-- <mat-hint align=\"end\">select source deviceId from the list ^</mat-hint> -->\n        </mat-form-field>\n\n        &nbsp; &nbsp;\n\n        <img class=\"pull-center\" style=\"margin: 15px 15px 0 0;\" src=\"./assets/images/right-arrow.png\" width=\"60px\">\n\n        <!-- <div class=\"divider\"></div> -->\n        <mat-form-field>  \n            <mat-select placeholder=\"Select Target Device Id\" [(value)]=\"selectedTargetDeviceId\">\n              <mat-option *ngFor=\"let option of dropdown1\"\n              value=\"{{ option }}\"> {{ option }}</mat-option>\n            </mat-select>\n          <!-- <mat-hint align=\"end\">select target deviceId from the list ^</mat-hint> -->\n        </mat-form-field>\n        &nbsp; &nbsp;\n        <button mat-button color=\"primary\" (click)=\"nextStep()\">Map</button>\n\n        <!-- <div><button mat-button (click)=\"addRowsToPanel1()\" color=\"green\">Add to List >></button></div> -->\n\n        <!-- <div *ngIf=\"(rows1.length > 0)\" align=\"center\">\n          <div><p>Added Device Info List</p></div>\n          <table border=\"1\">\n            <thead>\n              <tr>\n                <th *ngFor=\"let col of columns1\">{{col}} &nbsp;</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let row of rows1\">\n                  <td *ngFor=\"let col of columns1\" word-wrap: normal>\n                    {{ row[col] }}\n                  </td>\n              </tr>\n            </tbody>\n          </table>\n        </div> -->\n\n      <mat-action-row>\n        <button mat-button color=\"primary\" (click)=\"nextStep()\">Next</button>\n      </mat-action-row>\n    </mat-expansion-panel>\n  \n    <mat-expansion-panel [expanded]=\"step === 1\" (opened)=\"setStep(1)\" hideToggle>\n      <mat-expansion-panel-header>\n        <mat-panel-title>\n          <b>Feature Mapping</b>\n        </mat-panel-title>\n      </mat-expansion-panel-header>\n\n      &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;\n        <!-- <mat-form-field>  \n            <mat-select placeholder=\"Select Feature for Device Id:{{selectedSourceDeviceId}}\" [(value)]=\"selectedSourceDeviceId\">\n              <mat-option *ngFor=\"let option of dropdown1\"\n              value=\"{{ option }}\"> {{ option }}</mat-option>\n            </mat-select>\n           <mat-hint align=\"end\">select source deviceId from the list ^</mat-hint> \n        </mat-form-field> -->\n\n         <mat-form-field>\n          <mat-select placeholder=\"PNF Device Features\" (selectionChange)=\"doSomething($event)\" [formControl]=\"features\" multiple>\n            <mat-option *ngFor=\"let feature of featureList\" [value]=\"feature\">{{feature}}</mat-option>\n          </mat-select>\n         </mat-form-field>\n        <!-- <button mat-button color=\"primary\" (click)=\"foleSave()\">Create</button> -->\n        &nbsp; &nbsp;       \n\n      <!-- <mat-form-field>\n        <input matInput type=\"text\" formControlName=\"vnfValue\" placeholder=\"VNF Value\">\n      </mat-form-field> -->\n      <!-- <button mat-button (click)=\"addRowsToPanel2()\" color=\"green\">Add to List >> </button> -->\n\n      <!-- <div *ngIf=\"(rows2.length > 0)\" align=\"center\"> -->\n          <div align=\"center\">\n          <div><p>Added Data Mapper List</p></div>\n          <table border=\"1\">\n            <thead align=\"center\">\n              <tr>\n                <th *ngFor=\"let col of columns2\">{{col}} &nbsp;</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let row of rows2\" align=\"center\">\n                  <td *ngFor=\"let col of columns2\" word-wrap: normal style=\"background-color : rgb(235, 227, 227);\">\n                    <!-- {{ row[col] }} -->\n                     <div *ngIf=\"(col=='VNFvalue')\"> \n                        <input style=\"background-color : white;\" matInput type=\"text\" (change)=\"rowChange(row[col])\" value=\"{{row[col]}}\">\n                    </div> \n                    <!-- <div *ngIf!=(col=vnfValue)> -->\n                      <div *ngIf=\"(col!='VNFvalue')\">\n                        {{ row[col] }}\n                    </div>                   \n                  </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n\n      <mat-action-row>\n        <button mat-button color=\"warn\" (click)=\"prevStep()\">Previous</button>\n        <button mat-button color=\"primary\" (click)=\"nextStep()\">End</button>\n      </mat-action-row>\n    </mat-expansion-panel>  \n  </mat-accordion> <br>\n  <div>\n    <button (click)=\"onSubmit()\" class=\"btn_sub btn-success\">Run Script</button>&nbsp;\n    <button (click)=\"onRunScript()\" class=\"btn_sub btn-primary\">Create Files</button>    \n  </div>  \n</form>"

/***/ }),

/***/ "./src/app/config/config.component.ts":
/*!********************************************!*\
  !*** ./src/app/config/config.component.ts ***!
  \********************************************/
/*! exports provided: ConfigComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigComponent", function() { return ConfigComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_models */ "./src/app/_models/index.ts");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services */ "./src/app/_services/index.ts");
/* harmony import */ var file_saver___WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! file-saver/ */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver___WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(file_saver___WEBPACK_IMPORTED_MODULE_6__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ConfigComponent = /** @class */ (function () {
    function ConfigComponent(userService, formBuilder, fileCreationService, router, alertService, authenticationService, edaService) {
        this.userService = userService;
        this.formBuilder = formBuilder;
        this.fileCreationService = fileCreationService;
        this.router = router;
        this.alertService = alertService;
        this.authenticationService = authenticationService;
        this.edaService = edaService;
        this.users = [];
        this.loading = false;
        this.submitted = false;
        this.deviceInfoList = new Array();
        this.columns1 = ['PNFDeviceId', 'VNFDeviceId'];
        this.rows1 = [];
        this.dataMapperInfoList = new Array();
        this.columns2 = ['PNFMOID', 'PNFname', 'PNFvalue', 'VNFMOID', 'VNFname', 'VNFvalue'];
        this.rows2 = [];
        this.dropdown1 = [];
        this.selectedTargetDeviceId = "value_target";
        this.selectedSourceDeviceId = "value_source";
        this.featureList = [];
        this.features = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.panelOpenState = false;
        this.panel1items = [1];
        this.panel2items = [1];
        this.step = 0;
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    ConfigComponent.prototype.ngOnInit = function () {
        this.homeForm = this.formBuilder.group({
            pnfId: [],
            vnfId: [],
            pnfMoId: [],
            pnfName: [],
            pnfValue: [],
            vnfMoId: [],
            vnfName: [],
            vnfValue: []
        });
        this.loadAllUsers();
        this.getToken();
        this.getDevices();
    };
    ConfigComponent.prototype.doSomething = function () {
        var _this = this;
        this.rows2 = [];
        if (this.features != null) {
            this.features.value.forEach(function (element) {
                var row = [];
                var partsOfStr = element.split(',');
                row[_this.columns2[0]] = partsOfStr[0];
                row[_this.columns2[1]] = partsOfStr[1];
                row[_this.columns2[2]] = partsOfStr[2];
                row[_this.columns2[3]] = partsOfStr[0];
                ;
                row[_this.columns2[4]] = partsOfStr[1];
                row[_this.columns2[5]] = partsOfStr[2];
                _this.rows2.push(row);
            });
        }
        console.log("I am in do something ");
    };
    ConfigComponent.prototype.rowChange = function (event, value) {
        debugger;
        console.log("row updated" + event);
    };
    Object.defineProperty(ConfigComponent.prototype, "f", {
        // convenience getter for easy access to form fields
        get: function () { return this.homeForm.controls; },
        enumerable: true,
        configurable: true
    });
    ConfigComponent.prototype.onRunScript = function () {
        var _this = this;
        this.fileCreationService.runScript()
            .subscribe(function (data) {
            _this.alertService.success("Script execution triggered successfully");
            _this.ngOnInit();
            // this.router.navigate(['/login']);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    ConfigComponent.prototype.onSubmit = function () {
        var _this = this;
        var row = [];
        var deviceInfo = new _models__WEBPACK_IMPORTED_MODULE_4__["DeviceInfo"]();
        deviceInfo.pnfId = this.selectedSourceDeviceId;
        deviceInfo.vnfId = this.selectedTargetDeviceId;
        this.deviceInfoList.push(deviceInfo);
        this.rows2.forEach(function (elem) {
            var dataMapperInfo = new _models__WEBPACK_IMPORTED_MODULE_4__["DataMapperInfo"]();
            dataMapperInfo.pnfMoId = elem[_this.columns2[0]];
            dataMapperInfo.pnfName = elem[_this.columns2[1]];
            dataMapperInfo.pnfValue = elem[_this.columns2[2]];
            dataMapperInfo.vnfMoId = elem[_this.columns2[3]];
            dataMapperInfo.vnfName = elem[_this.columns2[4]];
            dataMapperInfo.vnfValue = elem[_this.columns2[5]];
            _this.dataMapperInfoList.push(dataMapperInfo);
        });
        var createFilesReq = new _models__WEBPACK_IMPORTED_MODULE_4__["CreateFilesReq"]();
        createFilesReq.deviceInfoList = this.deviceInfoList;
        createFilesReq.dataMapperInfoList = this.dataMapperInfoList;
        //createFilesReq.dataMapperInfoList = this.rows2;
        this.fileCreationService.createFiles(createFilesReq)
            .subscribe(function (data) {
            _this.alertService.success("Files created successfully");
            _this.ngOnInit();
            // this.router.navigate(['/login']);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    ConfigComponent.prototype.addRowsToPanel1 = function () {
        //this.panel1items.push(1);
        var deviceInfo = new _models__WEBPACK_IMPORTED_MODULE_4__["DeviceInfo"]();
        var row = [];
        deviceInfo.pnfId = this.f.pnfId.value;
        deviceInfo.vnfId = this.f.vnfId.value;
        if (deviceInfo.pnfId != null && deviceInfo.vnfId != null) {
            this.deviceInfoList.push(deviceInfo);
            row[this.columns1[0]] = deviceInfo.pnfId;
            row[this.columns1[1]] = deviceInfo.vnfId;
            this.rows1.push(row);
        }
        this.homeForm = this.formBuilder.group({
            pnfId: [],
            vnfId: [],
            pnfMoId: [],
            pnfName: [],
            pnfValue: [],
            vnfMoId: [],
            vnfName: [],
            vnfValue: []
        });
    };
    ConfigComponent.prototype.addRowsToPanel2 = function () {
        //this.panel2items.push(1);
        var dataMapperInfo = new _models__WEBPACK_IMPORTED_MODULE_4__["DataMapperInfo"]();
        var row = [];
        dataMapperInfo.pnfMoId = this.f.pnfMoId.value;
        dataMapperInfo.pnfName = this.f.pnfName.value;
        dataMapperInfo.pnfValue = this.f.pnfValue.value;
        dataMapperInfo.vnfMoId = this.f.vnfMoId.value;
        dataMapperInfo.vnfName = this.f.vnfName.value;
        dataMapperInfo.vnfValue = this.f.vnfValue.value;
        if (dataMapperInfo.pnfMoId != null && dataMapperInfo.pnfName != null && dataMapperInfo.pnfValue != null
            && dataMapperInfo.vnfMoId != null && dataMapperInfo.vnfName != null && dataMapperInfo.vnfValue != null) {
            this.dataMapperInfoList.push(dataMapperInfo);
            row[this.columns2[0]] = dataMapperInfo.pnfMoId;
            row[this.columns2[1]] = dataMapperInfo.pnfName;
            row[this.columns2[2]] = dataMapperInfo.pnfValue;
            row[this.columns2[3]] = dataMapperInfo.vnfMoId;
            row[this.columns2[4]] = dataMapperInfo.vnfName;
            row[this.columns2[5]] = dataMapperInfo.vnfValue;
            this.rows2.push(row);
        }
        this.homeForm = this.formBuilder.group({
            pnfId: [],
            vnfId: [],
            pnfMoId: [],
            pnfName: [],
            pnfValue: [],
            vnfMoId: [],
            vnfName: [],
            vnfValue: []
        });
    };
    ConfigComponent.prototype.deleteUser = function (id) {
        var _this = this;
        this.userService.delete(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["first"])()).subscribe(function () {
            _this.loadAllUsers();
        });
    };
    ConfigComponent.prototype.loadAllUsers = function () {
        var _this = this;
        this.userService.getAll().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["first"])()).subscribe(function (users) {
            _this.users = users;
        });
    };
    ConfigComponent.prototype.setStep = function (index) {
        this.step = index;
    };
    ConfigComponent.prototype.nextStep = function () {
        this.step++;
        console.log("Selected Source Id is: " + this.selectedSourceDeviceId);
        this.getAttributes(this.selectedSourceDeviceId, true);
        console.log("Selected Target Id is: " + this.selectedTargetDeviceId);
        this.getAttributes(this.selectedTargetDeviceId, false);
    };
    ConfigComponent.prototype.prevStep = function () {
        this.step--;
    };
    ConfigComponent.prototype.foleSave = function () {
        var blob = new Blob(["fgjghjggjfggjh", "dkjafhdsjdsfjaf"], { type: 'text/plain' });
        Object(file_saver___WEBPACK_IMPORTED_MODULE_6__["saveAs"])(blob, "myFile");
    };
    ConfigComponent.prototype.getToken = function () {
        var _this = this;
        this.authenticationService.authToken()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["first"])())
            .subscribe(function (data) {
            console.log(_this.printData(data));
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    ConfigComponent.prototype.printData = function (data) {
        localStorage.setItem('accessToekn', data['access_token']);
        console.log("Access token :" + data['access_token']);
    };
    ConfigComponent.prototype.getDevices = function () {
        var _this = this;
        this.edaService.deviceList()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["first"])())
            .subscribe(function (resp) {
            _this.printDeviceList(resp);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    ConfigComponent.prototype.printDeviceList = function (data) {
        var _this = this;
        data.listItems.forEach(function (element) {
            _this.dropdown1.push(element.masterIdentifier);
        });
    };
    ConfigComponent.prototype.getAttributes = function (deviceId, isSource) {
        var _this = this;
        this.edaService.attributeList(deviceId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["first"])())
            .subscribe(function (resp) {
            _this.printAttrbuteList(resp, isSource);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    ConfigComponent.prototype.printAttrbuteList = function (data, isSource) {
        var _this = this;
        for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
            var element = data_1[_i];
            if (element.ospfId != null) {
                if (isSource) {
                    if (element.networks != null && element.networks.network.length > 0) {
                        element.networks.network.forEach(function (element1, index) {
                            if (element1.ipAddress != null) {
                                _this.featureList.push("ospfId, networks.network[" + index + "].ipaddress," + element1.ipAddress);
                            }
                            if (element1.areaId != null) {
                                _this.featureList.push("ospfId,  networks.network[" + index + "].areaId," + element1.areaId);
                            }
                            if (element1.wildcardMask != null) {
                                _this.featureList.push("ospfId,  networks.network[" + index + "].wildcardMask," + element1.wildcardMask);
                            }
                        });
                    }
                    if (element.routerId != null) {
                        this.featureList.push("ospfId, routerId," + element.routerId);
                    }
                }
            }
        }
    };
    ConfigComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({ template: __webpack_require__(/*! ./config.component.html */ "./src/app/config/config.component.html"), styles: [__webpack_require__(/*! ./config.component.css */ "./src/app/config/config.component.css")] }),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_5__["UserService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _services__WEBPACK_IMPORTED_MODULE_5__["FileCreationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services__WEBPACK_IMPORTED_MODULE_5__["AlertService"],
            _services__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"],
            _services__WEBPACK_IMPORTED_MODULE_5__["EDAService"]])
    ], ConfigComponent);
    return ConfigComponent;
}());



/***/ }),

/***/ "./src/app/config/index.ts":
/*!*********************************!*\
  !*** ./src/app/config/index.ts ***!
  \*********************************/
/*! exports provided: ConfigComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _config_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./config.component */ "./src/app/config/config.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ConfigComponent", function() { return _config_component__WEBPACK_IMPORTED_MODULE_0__["ConfigComponent"]; });




/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-headers-align .mat-expansion-panel-header-title,\r\n.example-headers-align .mat-expansion-panel-header-description {\r\n  flex-basis: 0;\r\n}\r\n\r\n.example-headers-align .mat-expansion-panel-header-description {\r\n  justify-content: space-between;\r\n  align-items: center;\r\n}\r\n\r\n.divider{\r\n  width:5px;\r\n  height:auto;\r\n  display:inline-block;\r\n}\r\n\r\n.mat-expansion-panel-content {\r\n  font: 400 12px/20px Roboto,\"Helvetica Neue\",sans-serif;\r\n}\r\n\r\n.mat-form-field-appearance-legacy .mat-form-field-infix {\r\n  padding: .4375em 0;\r\n}\r\n\r\n.mat-form-field-appearance-legacy .mat-form-field-infix {\r\n  padding: .4375em 0;\r\n}\r\n\r\n.mat-form-field-infix {\r\n  display: block;\r\n  position: relative;\r\n  flex: auto;\r\n  min-width: 0;\r\n  width: 150px;\r\n}\r\n\r\n.btn_sub {\r\n  display: inline-block;\r\n  font-weight: 400;\r\n  text-align: center;\r\n  white-space: nowrap;\r\n  vertical-align: middle;\r\n  -webkit-user-select: none;\r\n  -moz-user-select: none;\r\n  -ms-user-select: none;\r\n  user-select: none;\r\n  border: 1px solid transparent;\r\n  padding: .375rem .75rem;\r\n  font-size: 1rem;\r\n  line-height: 1.5;\r\n  float: right;\r\n  border-radius: .25rem;\r\n  transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;\r\n}\r\n\r\n/* .btn-primary {\r\n  color: #fff;\r\n  background-color: gray;\r\n  border-color: #007bff;\r\n} */"

/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [ngStyle]=\"{'background' : 'url(../../assets/images/ericsson_logo.png)'}\"></div>\n<form [formGroup]=\"homeForm\" width=\"100%\" border=\"1\">\n<mat-accordion class=\"example-headers-align\">\n    <mat-expansion-panel [expanded]=\"step === 0\" (opened)=\"setStep(0)\" hideToggle>\n      <mat-expansion-panel-header align=\"center\">\n        <mat-panel-title>\n          <b>Device data</b>\n        </mat-panel-title>        \n      </mat-expansion-panel-header>\n      \n        <mat-form-field>\n          <input matInput type=\"text\" formControlName=\"pnfId\" placeholder=\"PNF Device ID\">\n        </mat-form-field>\n        &nbsp;\n        <mat-form-field>\n          <input matInput type=\"text\" formControlName=\"vnfId\" placeholder=\"VNF Device ID\">\n        </mat-form-field> &nbsp;\n        <!-- <button mat-button (click)=\"addRowsToPanel1()\" color=\"green\">+</button> -->\n        <div><button mat-button (click)=\"addRowsToPanel1()\" color=\"green\">Add to List >></button></div>\n\n        <div *ngIf=\"(rows1.length > 0)\" align=\"center\">\n          <div><p>Added Device Info List</p></div>\n          <table border=\"1\">\n            <thead>\n              <tr>\n                <th *ngFor=\"let col of columns1\">{{col}} &nbsp;</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let row of rows1\">\n                  <td *ngFor=\"let col of columns1\" word-wrap: normal>\n                    {{ row[col] }}\n                  </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n\n\n      <!-- <div *ngFor=\"let item of panel1items; let i = index\">\n        <mat-form-field>\n          <input matInput type=\"text\" formControlName=\"pnfId\" placeholder=\"PNF Device ID\">\n        </mat-form-field>\n        &nbsp;\n        <mat-form-field>\n          <input matInput type=\"text\" formControlName=\"vnfId\" placeholder=\"VNF Device ID\">\n        </mat-form-field>\n      </div>\n      <button mat-button (click)=\"addRowsToPanel1()\" color=\"green\">Add More Data >></button> -->\n  \n      <mat-action-row>\n        <button mat-button color=\"primary\" (click)=\"nextStep()\">Next</button>\n      </mat-action-row>\n    </mat-expansion-panel>\n  \n    <mat-expansion-panel [expanded]=\"step === 1\" (opened)=\"setStep(1)\" hideToggle>\n      <mat-expansion-panel-header>\n        <mat-panel-title>\n          <b>Data Mapper Information</b>\n        </mat-panel-title>\n        <!-- <mat-panel-description>\n          Enter Data Mapper Information \n          <mat-icon>map</mat-icon> \n        </mat-panel-description> -->\n      </mat-expansion-panel-header>\n  \n      <mat-form-field>\n        <input matInput type=\"text\" formControlName=\"pnfMoId\" placeholder=\"PNF MO ID\">\n      </mat-form-field> \n\n      <mat-form-field>\n        <input matInput type=\"text\" formControlName=\"pnfName\"  placeholder=\"PNF Name\">\n      </mat-form-field> \n      \n      <mat-form-field>\n        <input matInput type=\"text\" formControlName=\"pnfValue\"  placeholder=\"PNF Value\">\n      </mat-form-field> \n\n      <mat-form-field>\n        <input matInput type=\"text\" formControlName=\"vnfMoId\"  placeholder=\"VNF MO ID\">\n      </mat-form-field> \n\n      <mat-form-field>\n        <input matInput type=\"text\" formControlName=\"vnfName\" placeholder=\"VNF NAME\">\n      </mat-form-field> \n\n      <mat-form-field>\n        <input matInput type=\"text\" formControlName=\"vnfValue\" placeholder=\"VNF Value\">\n      </mat-form-field>\n      <button mat-button (click)=\"addRowsToPanel2()\" color=\"green\">Add to List >> </button>\n\n      <div *ngIf=\"(rows2.length > 0)\" align=\"center\">\n          <div><p>Added Data Mapper Info List</p></div>\n          <table border=\"1\">\n            <thead>\n              <tr>\n                <th *ngFor=\"let col of columns2\">{{col}} &nbsp;</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let row of rows2\">\n                  <td *ngFor=\"let col of columns2\" word-wrap: normal>\n                    {{ row[col] }}\n                  </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n\n      <mat-action-row>\n        <button mat-button color=\"warn\" (click)=\"prevStep()\">Previous</button>\n        <button mat-button color=\"primary\" (click)=\"nextStep()\">End</button>\n      </mat-action-row>\n    </mat-expansion-panel>  \n  </mat-accordion> <br>\n  <div>\n      <button (click)=\"onSubmit()\" class=\"btn_sub btn-primary\">Submit</button>\n  </div>\n</form>"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_models */ "./src/app/_models/index.ts");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomeComponent = /** @class */ (function () {
    function HomeComponent(userService, formBuilder, fileCreationService, router, alertService) {
        this.userService = userService;
        this.formBuilder = formBuilder;
        this.fileCreationService = fileCreationService;
        this.router = router;
        this.alertService = alertService;
        this.users = [];
        this.loading = false;
        this.submitted = false;
        this.deviceInfoList = new Array();
        this.columns1 = ['PNFDeviceId', 'VNFDeviceId'];
        this.rows1 = [];
        this.dataMapperInfoList = new Array();
        this.columns2 = ['PNFMOID', 'PNFname', 'PNFvalue', 'VNFMOID', 'VNFname', 'VNFvalue'];
        this.rows2 = [];
        this.panelOpenState = false;
        this.panel1items = [1];
        this.panel2items = [1];
        this.step = 0;
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.homeForm = this.formBuilder.group({
            pnfId: [],
            vnfId: [],
            pnfMoId: [],
            pnfName: [],
            pnfValue: [],
            vnfMoId: [],
            vnfName: [],
            vnfValue: []
        });
        this.loadAllUsers();
    };
    Object.defineProperty(HomeComponent.prototype, "f", {
        // convenience getter for easy access to form fields
        get: function () { return this.homeForm.controls; },
        enumerable: true,
        configurable: true
    });
    HomeComponent.prototype.onSubmit = function () {
        var _this = this;
        var createFilesReq = new _models__WEBPACK_IMPORTED_MODULE_4__["CreateFilesReq"]();
        createFilesReq.deviceInfoList = this.deviceInfoList;
        createFilesReq.dataMapperInfoList = this.dataMapperInfoList;
        this.fileCreationService.createFiles(createFilesReq)
            .subscribe(function (data) {
            _this.alertService.success("Files created successfully");
            _this.ngOnInit();
            // this.router.navigate(['/login']);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    HomeComponent.prototype.addRowsToPanel1 = function () {
        //this.panel1items.push(1);
        var deviceInfo = new _models__WEBPACK_IMPORTED_MODULE_4__["DeviceInfo"]();
        var row = [];
        deviceInfo.pnfId = this.f.pnfId.value;
        deviceInfo.vnfId = this.f.vnfId.value;
        if (deviceInfo.pnfId != null && deviceInfo.vnfId != null) {
            this.deviceInfoList.push(deviceInfo);
            row[this.columns1[0]] = deviceInfo.pnfId;
            row[this.columns1[1]] = deviceInfo.vnfId;
            this.rows1.push(row);
        }
        this.homeForm = this.formBuilder.group({
            pnfId: [],
            vnfId: [],
            pnfMoId: [],
            pnfName: [],
            pnfValue: [],
            vnfMoId: [],
            vnfName: [],
            vnfValue: []
        });
    };
    HomeComponent.prototype.addRowsToPanel2 = function () {
        //this.panel2items.push(1);
        var dataMapperInfo = new _models__WEBPACK_IMPORTED_MODULE_4__["DataMapperInfo"]();
        var row = [];
        dataMapperInfo.pnfMoId = this.f.pnfMoId.value;
        dataMapperInfo.pnfName = this.f.pnfName.value;
        dataMapperInfo.pnfValue = this.f.pnfValue.value;
        dataMapperInfo.vnfMoId = this.f.vnfMoId.value;
        dataMapperInfo.vnfName = this.f.vnfName.value;
        dataMapperInfo.vnfValue = this.f.vnfValue.value;
        if (dataMapperInfo.pnfMoId != null && dataMapperInfo.pnfName != null && dataMapperInfo.pnfValue != null
            && dataMapperInfo.vnfMoId != null && dataMapperInfo.vnfName != null && dataMapperInfo.vnfValue != null) {
            this.dataMapperInfoList.push(dataMapperInfo);
            row[this.columns2[0]] = dataMapperInfo.pnfMoId;
            row[this.columns2[1]] = dataMapperInfo.pnfName;
            row[this.columns2[2]] = dataMapperInfo.pnfValue;
            row[this.columns2[3]] = dataMapperInfo.vnfMoId;
            row[this.columns2[4]] = dataMapperInfo.vnfName;
            row[this.columns2[5]] = dataMapperInfo.vnfValue;
            this.rows2.push(row);
        }
        this.homeForm = this.formBuilder.group({
            pnfId: [],
            vnfId: [],
            pnfMoId: [],
            pnfName: [],
            pnfValue: [],
            vnfMoId: [],
            vnfName: [],
            vnfValue: []
        });
    };
    HomeComponent.prototype.deleteUser = function (id) {
        var _this = this;
        this.userService.delete(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["first"])()).subscribe(function () {
            _this.loadAllUsers();
        });
    };
    HomeComponent.prototype.loadAllUsers = function () {
        var _this = this;
        this.userService.getAll().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["first"])()).subscribe(function (users) {
            _this.users = users;
        });
    };
    HomeComponent.prototype.setStep = function (index) {
        this.step = index;
    };
    HomeComponent.prototype.nextStep = function () {
        this.step++;
    };
    HomeComponent.prototype.prevStep = function () {
        this.step--;
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({ template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"), styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")] }),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_5__["UserService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _services__WEBPACK_IMPORTED_MODULE_5__["FileCreationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services__WEBPACK_IMPORTED_MODULE_5__["AlertService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/home/index.ts":
/*!*******************************!*\
  !*** ./src/app/home/index.ts ***!
  \*******************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.component */ "./src/app/home/home.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return _home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"]; });




/***/ }),

/***/ "./src/app/login/index.ts":
/*!********************************!*\
  !*** ./src/app/login/index.ts ***!
  \********************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.component */ "./src/app/login/login.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return _login_component__WEBPACK_IMPORTED_MODULE_0__["LoginComponent"]; });




/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-control {\r\n    display: block;\r\n    width: 30%;\r\n    padding: .375rem .75rem;\r\n    font-size: 1rem;\r\n    line-height: 1.5;\r\n    color: #495057;\r\n    background-color: #fff;\r\n    background-clip: padding-box;\r\n    border: 1px solid #ced4da;\r\n    border-radius: .25rem;\r\n    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;\r\n}"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<h2 align=\"center\">Login</h2>-->\n<form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\" width=\"100%\" border=\"1\">\n    <div style=\"border:0px solid gray\">\n\n    <div class=\"form-group\" align=\"center\">\n        <mat-form-field>\n            <input matInput type=\"text\" formControlName=\"username\" placeholder=\"Username\" [ngClass]=\"{ 'is-invalid': submitted && f.username.errors }\">\n        </mat-form-field>\n        <!-- <label for=\"username\">Username</label>\n        <input type=\"text\" formControlName=\"username\" class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted && f.username.errors }\" /> -->\n        <div *ngIf=\"submitted && f.username.errors\" class=\"invalid-feedback\">\n            <div *ngIf=\"f.username.errors.required\">Username is required</div>\n        </div>\n    </div>\n    <div class=\"form-group\" align=\"center\">\n        <mat-form-field >\n            <input matInput type=\"password\" formControlName=\"password\" placeholder=\"Password\" [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\">\n        </mat-form-field>\n        <!-- <label for=\"password\">Password</label>\n        <input type=\"password\" formControlName=\"password\" class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" /> -->\n        <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n            <div *ngIf=\"f.password.errors.required\">Password is required</div>\n        </div>\n    </div>\n    <div class=\"form-group\" align=\"center\">\n        <button [disabled]=\"loading\" class=\"btn btn-primary\" (click)=\"onSubmit()\">Login</button>\n        <img *ngIf=\"loading\" src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\" />\n        <!--<a [routerLink]=\"['/register']\" class=\"btn btn-link\">Register</a>-->\n    </div>\n    </div>  \n</form>\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(formBuilder, route, router, authenticationService, alertService) {
        this.formBuilder = formBuilder;
        this.route = route;
        this.router = router;
        this.authenticationService = authenticationService;
        this.alertService = alertService;
        this.loading = false;
        this.submitted = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.loginForm = this.formBuilder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
        // reset login status
        this.authenticationService.logout();
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/config';
    };
    Object.defineProperty(LoginComponent.prototype, "f", {
        // convenience getter for easy access to form fields
        get: function () { return this.loginForm.controls; },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.loading = true;
        console.log("user name:" + this.f.username.value);
        console.log("password:" + this.f.password.value);
        this.authenticationService.login(this.f.username.value, this.f.password.value)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])())
            .subscribe(function (data) {
            _this.router.navigate([_this.returnUrl]);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({ template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"), styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")] }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"],
            _services__WEBPACK_IMPORTED_MODULE_4__["AlertService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/register/index.ts":
/*!***********************************!*\
  !*** ./src/app/register/index.ts ***!
  \***********************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _register_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./register.component */ "./src/app/register/register.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return _register_component__WEBPACK_IMPORTED_MODULE_0__["RegisterComponent"]; });




/***/ }),

/***/ "./src/app/register/register.component.html":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>Register</h2>\n<form [formGroup]=\"registerForm\" (ngSubmit)=\"onSubmit()\">\n    <div class=\"form-group\">\n        <label for=\"firstName\">First Name</label>\n        <input type=\"text\" formControlName=\"firstName\" class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted && f.firstName.errors }\" />\n        <div *ngIf=\"submitted && f.firstName.errors\" class=\"invalid-feedback\">\n            <div *ngIf=\"f.firstName.errors.required\">First Name is required</div>\n        </div>\n    </div>\n    <div class=\"form-group\">\n        <label for=\"lastName\">Last Name</label>\n        <input type=\"text\" formControlName=\"lastName\" class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted && f.lastName.errors }\" />\n        <div *ngIf=\"submitted && f.lastName.errors\" class=\"invalid-feedback\">\n            <div *ngIf=\"f.lastName.errors.required\">Last Name is required</div>\n        </div>\n    </div>\n    <div class=\"form-group\">\n        <label for=\"username\">Username</label>\n        <input type=\"text\" formControlName=\"username\" class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted && f.username.errors }\" />\n        <div *ngIf=\"submitted && f.username.errors\" class=\"invalid-feedback\">\n            <div *ngIf=\"f.username.errors.required\">Username is required</div>\n        </div>\n    </div>\n    <div class=\"form-group\">\n        <label for=\"password\">Password</label>\n        <input type=\"password\" formControlName=\"password\" class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" />\n        <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n            <div *ngIf=\"f.password.errors.required\">Password is required</div>\n            <div *ngIf=\"f.password.errors.minlength\">Password must be at least 6 characters</div>\n        </div>\n    </div>\n    <div class=\"form-group\">\n        <button [disabled]=\"loading\" class=\"btn btn-primary\">Register</button>\n        <img *ngIf=\"loading\" src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\" />\n        <a [routerLink]=\"['/login']\" class=\"btn btn-link\">Cancel</a>\n    </div>\n</form>\n"

/***/ }),

/***/ "./src/app/register/register.component.ts":
/*!************************************************!*\
  !*** ./src/app/register/register.component.ts ***!
  \************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(formBuilder, router, userService, alertService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.userService = userService;
        this.alertService = alertService;
        this.loading = false;
        this.submitted = false;
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.registerForm = this.formBuilder.group({
            firstName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            lastName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)]]
        });
    };
    Object.defineProperty(RegisterComponent.prototype, "f", {
        // convenience getter for easy access to form fields
        get: function () { return this.registerForm.controls; },
        enumerable: true,
        configurable: true
    });
    RegisterComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
        this.loading = true;
        this.userService.register(this.registerForm.value)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])())
            .subscribe(function (data) {
            _this.alertService.success('Registration successful', true);
            _this.router.navigate(['/login']);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({ template: __webpack_require__(/*! ./register.component.html */ "./src/app/register/register.component.html") }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services__WEBPACK_IMPORTED_MODULE_4__["UserService"],
            _services__WEBPACK_IMPORTED_MODULE_4__["AlertService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/welcome/index.ts":
/*!**********************************!*\
  !*** ./src/app/welcome/index.ts ***!
  \**********************************/
/*! exports provided: WelcomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _welcome_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./welcome.component */ "./src/app/welcome/welcome.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "WelcomeComponent", function() { return _welcome_component__WEBPACK_IMPORTED_MODULE_0__["WelcomeComponent"]; });




/***/ }),

/***/ "./src/app/welcome/welcome.component.html":
/*!************************************************!*\
  !*** ./src/app/welcome/welcome.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<table border=\"0\" align=\"center\" width=\"100%\">  \n    <tr>\n      <td colspan=\"2\" align=\"center\">\n        <h2>Welcome to {{ title }}!</h2>\n      </td>\n    </tr><br>\n    <tr>\n      <td colspan=\"2\" align=\"center\">\n        <img width=\"20%\" alt=\"Ericsson Logo\" src=\"./assets/images/ericsson-logo.jpg\">\n      </td>\n    </tr>\n    <tr>\n        <td cellspacing=\"2\" colspan=\"2\" align=\"center\">\n            <button>\n                    <a href=\"login\">Login</a>\n            </button>\n        </td>\n    </tr>\n    <!--\n    <tr>\n      <td colspan=\"2\">\n        <h3 align=\"center\">Here are some links to help you start: </h3>\n      </td>\n    </tr>\n    <tr>\n      <td>\n        <h4 align=\"center\"><a target=\"_blank\" rel=\"noopener\" href=\"https://www.ericsson.com\">Ericsson</a></h4>\n      </td>\n      <td>\n        <h4 align=\"center\"><a target=\"_blank\" rel=\"noopener\" href=\"https://www.att.com\">AT&T</a></h4>\n      </td>\n    </tr>\n    -->\n  </table>\n  \n  \n  "

/***/ }),

/***/ "./src/app/welcome/welcome.component.ts":
/*!**********************************************!*\
  !*** ./src/app/welcome/welcome.component.ts ***!
  \**********************************************/
/*! exports provided: WelcomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomeComponent", function() { return WelcomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var WelcomeComponent = /** @class */ (function () {
    function WelcomeComponent() {
        this.title = 'Ericsson - AT&T Demo project';
    }
    WelcomeComponent.prototype.ngOnInit = function () {
    };
    WelcomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({ template: __webpack_require__(/*! ./welcome.component.html */ "./src/app/welcome/welcome.component.html") })
    ], WelcomeComponent);
    return WelcomeComponent;
}());



/***/ }),

/***/ "./src/app/workflow/index.ts":
/*!***********************************!*\
  !*** ./src/app/workflow/index.ts ***!
  \***********************************/
/*! exports provided: WorkflowComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _workflow_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./workflow.component */ "./src/app/workflow/workflow.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "WorkflowComponent", function() { return _workflow_component__WEBPACK_IMPORTED_MODULE_0__["WorkflowComponent"]; });




/***/ }),

/***/ "./src/app/workflow/workflow.component.css":
/*!*************************************************!*\
  !*** ./src/app/workflow/workflow.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-headers-align .mat-expansion-panel-header-title,\r\n.example-headers-align .mat-expansion-panel-header-description {\r\n  flex-basis: 0;\r\n}\r\n\r\n.example-headers-align .mat-expansion-panel-header-description {\r\n  justify-content: space-between;\r\n  align-items: center;\r\n}\r\n\r\n.divider{\r\n  width:5px;\r\n  height:auto;\r\n  display:inline-block;\r\n}\r\n\r\n.mat-expansion-panel-content {\r\n  font: 400 12px/20px Roboto,\"Helvetica Neue\",sans-serif;\r\n}\r\n\r\n.mat-form-field-appearance-legacy .mat-form-field-infix {\r\n  padding: .4375em 0;\r\n}\r\n\r\n.mat-form-field-appearance-legacy .mat-form-field-infix {\r\n  padding: .4375em 0;\r\n}\r\n\r\n.mat-form-field-infix {\r\n  display: block;\r\n  position: relative;\r\n  flex: auto;\r\n  min-width: 0;\r\n  width: 150px;\r\n}\r\n\r\n.btn_sub {\r\n  display: inline-block;\r\n  font-weight: 400;\r\n  text-align: center;\r\n  white-space: nowrap;\r\n  vertical-align: middle;\r\n  -webkit-user-select: none;\r\n  -moz-user-select: none;\r\n  -ms-user-select: none;\r\n  user-select: none;\r\n  border: 1px solid transparent;\r\n  padding: .375rem .75rem;\r\n  font-size: 1rem;\r\n  line-height: 1.5;\r\n  float: right;\r\n  border-radius: .25rem;\r\n  transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;\r\n}\r\n\r\n/* .btn-primary {\r\n  color: #fff;\r\n  background-color: gray;\r\n  border-color: #007bff;\r\n} */"

/***/ }),

/***/ "./src/app/workflow/workflow.component.html":
/*!**************************************************!*\
  !*** ./src/app/workflow/workflow.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"homeForm\" width=\"100%\" border=\"1\">\n<mat-accordion class=\"example-headers-align\">\n    <mat-expansion-panel [expanded]=\"step === 0\" (opened)=\"setStep(0)\" hideToggle>\n      <mat-expansion-panel-header align=\"center\">\n        <mat-panel-title>\n          <b>CAMUNDA Workflow for use case one</b>\n        </mat-panel-title>        \n      </mat-expansion-panel-header>\n      \n      &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;\n      <img class=\"pull-center\" style=\"margin: 15px 15px 0 0;\" src=\"./assets/images/uc1-workflow.PNG\" width=\"600px\">\n        \n      <div> \n        <button (click)=\"startwf1()\" class=\"btn_sub btn-light\">Start Workflow</button> \n      </div>\n\n      <mat-action-row>\n        <button mat-button color=\"primary\" (click)=\"nextStep()\">Next</button>\n      </mat-action-row>\n    </mat-expansion-panel>\n  \n    <mat-expansion-panel [expanded]=\"step === 1\" (opened)=\"setStep(1)\" hideToggle>\n      <mat-expansion-panel-header>\n        <mat-panel-title>\n          <b>CAMUNDA Workflow for use case two</b>\n        </mat-panel-title>\n      </mat-expansion-panel-header>\n  \n      &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;\n      <img class=\"pull-center\" style=\"margin: 15px 15px 0 0;\" src=\"./assets/images/uc2-workflow.PNG\" width=\"600px\">\n      \n      <div> \n        <button (click)=\"startwf2()\" class=\"btn_sub btn-light\">Start Workflow</button> \n      </div>\n\n      <mat-action-row>\n        <button mat-button color=\"warn\" (click)=\"prevStep()\">Previous</button>\n        <button mat-button color=\"primary\" (click)=\"nextStep()\">End</button>\n      </mat-action-row>\n    </mat-expansion-panel>  \n  </mat-accordion> <br>\n  <div>\n      <button (click)=\"onSubmit()\" class=\"btn_sub btn-primary\">Submit</button>\n  </div>\n</form>"

/***/ }),

/***/ "./src/app/workflow/workflow.component.ts":
/*!************************************************!*\
  !*** ./src/app/workflow/workflow.component.ts ***!
  \************************************************/
/*! exports provided: WorkflowComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WorkflowComponent", function() { return WorkflowComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_models */ "./src/app/_models/index.ts");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var WorkflowComponent = /** @class */ (function () {
    function WorkflowComponent(userService, formBuilder, fileCreationService, router, alertService) {
        this.userService = userService;
        this.formBuilder = formBuilder;
        this.fileCreationService = fileCreationService;
        this.router = router;
        this.alertService = alertService;
        this.users = [];
        this.loading = false;
        this.submitted = false;
        this.deviceInfoList = new Array();
        this.columns1 = ['PNFDeviceId', 'VNFDeviceId'];
        this.rows1 = [];
        this.dataMapperInfoList = new Array();
        this.columns2 = ['PNFMOID', 'PNFname', 'PNFvalue', 'VNFMOID', 'VNFname', 'VNFvalue'];
        this.rows2 = [];
        this.panelOpenState = false;
        this.panel1items = [1];
        this.panel2items = [1];
        this.step = 0;
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    WorkflowComponent.prototype.ngOnInit = function () {
        this.homeForm = this.formBuilder.group({
            pnfId: [],
            vnfId: [],
            pnfMoId: [],
            pnfName: [],
            pnfValue: [],
            vnfMoId: [],
            vnfName: [],
            vnfValue: []
        });
        this.loadAllUsers();
    };
    Object.defineProperty(WorkflowComponent.prototype, "f", {
        // convenience getter for easy access to form fields
        get: function () { return this.homeForm.controls; },
        enumerable: true,
        configurable: true
    });
    WorkflowComponent.prototype.onSubmit = function () {
        var _this = this;
        var createFilesReq = new _models__WEBPACK_IMPORTED_MODULE_4__["CreateFilesReq"]();
        createFilesReq.deviceInfoList = this.deviceInfoList;
        createFilesReq.dataMapperInfoList = this.dataMapperInfoList;
        this.fileCreationService.createFiles(createFilesReq)
            .subscribe(function (data) {
            _this.alertService.success("Files created successfully");
            _this.ngOnInit();
            // this.router.navigate(['/login']);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    WorkflowComponent.prototype.addRowsToPanel1 = function () {
        //this.panel1items.push(1);
        var deviceInfo = new _models__WEBPACK_IMPORTED_MODULE_4__["DeviceInfo"]();
        var row = [];
        deviceInfo.pnfId = this.f.pnfId.value;
        deviceInfo.vnfId = this.f.vnfId.value;
        if (deviceInfo.pnfId != null && deviceInfo.vnfId != null) {
            this.deviceInfoList.push(deviceInfo);
            row[this.columns1[0]] = deviceInfo.pnfId;
            row[this.columns1[1]] = deviceInfo.vnfId;
            this.rows1.push(row);
        }
        this.homeForm = this.formBuilder.group({
            pnfId: [],
            vnfId: [],
            pnfMoId: [],
            pnfName: [],
            pnfValue: [],
            vnfMoId: [],
            vnfName: [],
            vnfValue: []
        });
    };
    WorkflowComponent.prototype.addRowsToPanel2 = function () {
        //this.panel2items.push(1);
        var dataMapperInfo = new _models__WEBPACK_IMPORTED_MODULE_4__["DataMapperInfo"]();
        var row = [];
        dataMapperInfo.pnfMoId = this.f.pnfMoId.value;
        dataMapperInfo.pnfName = this.f.pnfName.value;
        dataMapperInfo.pnfValue = this.f.pnfValue.value;
        dataMapperInfo.vnfMoId = this.f.vnfMoId.value;
        dataMapperInfo.vnfName = this.f.vnfName.value;
        dataMapperInfo.vnfValue = this.f.vnfValue.value;
        if (dataMapperInfo.pnfMoId != null && dataMapperInfo.pnfName != null && dataMapperInfo.pnfValue != null
            && dataMapperInfo.vnfMoId != null && dataMapperInfo.vnfName != null && dataMapperInfo.vnfValue != null) {
            this.dataMapperInfoList.push(dataMapperInfo);
            row[this.columns2[0]] = dataMapperInfo.pnfMoId;
            row[this.columns2[1]] = dataMapperInfo.pnfName;
            row[this.columns2[2]] = dataMapperInfo.pnfValue;
            row[this.columns2[3]] = dataMapperInfo.vnfMoId;
            row[this.columns2[4]] = dataMapperInfo.vnfName;
            row[this.columns2[5]] = dataMapperInfo.vnfValue;
            this.rows2.push(row);
        }
        this.homeForm = this.formBuilder.group({
            pnfId: [],
            vnfId: [],
            pnfMoId: [],
            pnfName: [],
            pnfValue: [],
            vnfMoId: [],
            vnfName: [],
            vnfValue: []
        });
    };
    WorkflowComponent.prototype.deleteUser = function (id) {
        var _this = this;
        this.userService.delete(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["first"])()).subscribe(function () {
            _this.loadAllUsers();
        });
    };
    WorkflowComponent.prototype.loadAllUsers = function () {
        var _this = this;
        this.userService.getAll().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["first"])()).subscribe(function (users) {
            _this.users = users;
        });
    };
    WorkflowComponent.prototype.setStep = function (index) {
        this.step = index;
    };
    WorkflowComponent.prototype.nextStep = function () {
        this.step++;
    };
    WorkflowComponent.prototype.prevStep = function () {
        this.step--;
    };
    WorkflowComponent.prototype.startwf1 = function () {
        var _this = this;
        this.fileCreationService.startwf1()
            .subscribe(function (data) {
            _this.alertService.success("Workflow started successfully");
            _this.ngOnInit();
            // this.router.navigate(['/login']);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    WorkflowComponent.prototype.startwf2 = function () {
        var _this = this;
        this.fileCreationService.startwf2()
            .subscribe(function (data) {
            _this.alertService.success("Workflow started successfully");
            _this.ngOnInit();
            // this.router.navigate(['/login']);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    WorkflowComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({ template: __webpack_require__(/*! ./workflow.component.html */ "./src/app/workflow/workflow.component.html"), styles: [__webpack_require__(/*! ./workflow.component.css */ "./src/app/workflow/workflow.component.css")] }),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_5__["UserService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _services__WEBPACK_IMPORTED_MODULE_5__["FileCreationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services__WEBPACK_IMPORTED_MODULE_5__["AlertService"]])
    ], WorkflowComponent);
    return WorkflowComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    apiUrl: 'http://localhost:4000'
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Ravinder\ATT-Ericsson-Project\att-demo-web-project\tutorial-web\src\main\web\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map